#!/bin/bash
set -euxo pipefail

export PATH="/usr/lib64/ccache:${PATH}"

ccache -z
ccache -s

cd /ramdisk

tar xf "${CI_PROJECT_DIR}/kernel.tar.gz"

cp "${ARCH_CONFIG}.config" .config
make olddefconfig
make -j$(nproc) INSTALL_MOD_STRIP=1 targz-pkg 2>&1 | ts -s | tee -a build.log

xz -v build.log
mv linux-*.tar.gz build.log.xz "${CI_PROJECT_DIR}/"

ccache -s
