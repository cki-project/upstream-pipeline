#!/bin/bash
set -euxo pipefail

cd /ramdisk

git config --global user.name "CKI Pipeline"
git config --global user.email "cki-project@gitlab.com"
git clone --quiet --depth 1 --branch linux-5.1.y https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git

pushd linux
    CONFIG_ARCHES=( aarch64 ppc64le s390x x86_64 )
    for ARCH in "${CONFIG_ARCHES[@]}"; do
      curl -Ls --retry 5 --connect-timeout 30 --output ${ARCH}.config \
        https://gitlab.com/cki-project/kernel-configs/raw/master/${ARCH}.config
    done

    git add *.config
    git commit -m "Adding config files"
    git archive -o ${CI_PROJECT_DIR}/kernel.tar.gz HEAD
popd
